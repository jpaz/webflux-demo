package alpha.webflux.webflux.repository

import alpha.webflux.webflux.model.Message
import org.springframework.data.repository.reactive.ReactiveCrudRepository

interface MessageReactiveRepository: ReactiveCrudRepository<Message, String> {
}