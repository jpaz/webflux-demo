package alpha.webflux.webflux.controller

import alpha.webflux.webflux.model.Message
import alpha.webflux.webflux.service.MessageReactiveService
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping(value = ["/messages"])
class MessageController(private val messageReactiveService: MessageReactiveService) {

    @GetMapping(value = ["/stream"], produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun listen(): Flux<Message> {
        return Flux.interval(Duration.ofSeconds(1))
                .onBackpressureDrop()
                .map { t -> Message(t.toString(), "This is the $t message", Date()) }
    }

    @GetMapping
    fun findAll(): Flux<Message> {
        return messageReactiveService.findAll()
    }

    @GetMapping(value = ["/{id}"])
    fun findById(@PathVariable(value = "id") id: String): Mono<Message> {
        return messageReactiveService.findById(id)
    }

    @PostMapping
    fun save(@RequestBody @Valid message: Message): Mono<Message> {
        return messageReactiveService.save(message)
    }

    @PutMapping(value = ["/{id}"])
    fun update(@PathVariable(value = "id") @Valid id: String, @RequestBody message: Message): Mono<Message> {
        return messageReactiveService.update(id, message)
    }

    @DeleteMapping(value = ["/{id}"])
    fun delete(@PathVariable(value = "id") id: String): Mono<Message> {
        return messageReactiveService.delete(id)
    }
}