package alpha.webflux.webflux.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.util.*
import javax.validation.constraints.NotEmpty

@Document(collection = "messages")
data class Message(
        @Id
        val id: String? = UUID.randomUUID().toString(),
        @Field
        @NotEmpty
        var data: String,
        @Field
        var createAt: Date? = Date()
) {
    constructor(message: String) : this(null, message, null)
}