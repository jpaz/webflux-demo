package alpha.webflux.webflux.service.impl

import alpha.webflux.webflux.model.Message
import alpha.webflux.webflux.repository.MessageReactiveRepository
import alpha.webflux.webflux.service.MessageReactiveService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class MessageReactiveServiceImpl(private val messageReactiveRepository: MessageReactiveRepository) : MessageReactiveService {

    @Transactional(readOnly = true)
    override fun findAll(): Flux<Message> {
        return messageReactiveRepository.findAll()
    }

    @Transactional(readOnly = true)
    override fun findById(id: String): Mono<Message> {
        return messageReactiveRepository.findById(id)
    }

    @Transactional(readOnly = false)
    override fun save(message: Message): Mono<Message> {
        return messageReactiveRepository.save(message)
    }

    @Transactional(readOnly = false)
    override fun update(id: String, message: Message): Mono<Message> {
        return findById(id).flatMap { m ->
            if (m == null)
                return@flatMap Mono.empty<Message>()
            m.data = message.data
            return@flatMap messageReactiveRepository.save(m)
        }
    }

    @Transactional(readOnly = false)
    override fun delete(id: String): Mono<Message> {
        return findById(id).flatMap { m ->
            if (m == null)
                return@flatMap Mono.empty<Message>()
            return@flatMap messageReactiveRepository.deleteById(id).then(Mono.just(m))
        }
    }
}