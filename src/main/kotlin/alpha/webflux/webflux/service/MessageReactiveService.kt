package alpha.webflux.webflux.service

import alpha.webflux.webflux.model.Message
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface MessageReactiveService {
    fun findAll(): Flux<Message>
    fun findById(id: String): Mono<Message>
    fun save(message: Message): Mono<Message>
    fun update(id: String, message: Message): Mono<Message>
    fun delete(id: String): Mono<Message>
}