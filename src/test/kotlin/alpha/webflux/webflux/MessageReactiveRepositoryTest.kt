package alpha.webflux.webflux

import alpha.webflux.webflux.model.Message
import alpha.webflux.webflux.repository.MessageReactiveRepository
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import reactor.core.publisher.Flux
import reactor.test.StepVerifier

@SpringBootTest
class MessageReactiveRepositoryTest {

    @Autowired
    private lateinit var messageReactiveRepository: MessageReactiveRepository

    @BeforeEach
    fun setup() {
        messageReactiveRepository.deleteAll().block()
        Flux.just("Hello world!", "Hello world!", "Good night!").flatMap { m ->
            messageReactiveRepository.save(Message(m))
        }.blockLast()
    }

    @Test
    fun whenFindDuplicateMessageThenTwo() {
        val messages: Flux<Message> = messageReactiveRepository.findAll()
                .filter { m -> m.data == "Hello world!" }

        StepVerifier.create(messages)
                .expectNextCount(2)
                .verifyComplete()
    }

    @Test
    fun whenFindNoDuplicateMessageThenOne() {
        val messages: Flux<Message> = messageReactiveRepository.findAll()
                .filter { m -> m.data == "Good night!" }

        StepVerifier.create(messages)
                .expectNextCount(1)
                .verifyComplete()
    }
}
